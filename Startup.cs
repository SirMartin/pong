﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Pong.Startup))]
namespace Pong
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
