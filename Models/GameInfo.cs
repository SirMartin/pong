﻿using System;

namespace Pong.Models
{
    public class GameInfo
    {
        public Guid Id { get; set; }
        public Player player1 { get; set; }
        public Player player2 { get; set; }
        public Ball ball { get; set; }
        public bool IsStarted { get; set; }
    }

    public class Player
    {
        public Player(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        public string id { get; set; }
        public string name { get; set; }
        public int score { get; set; }
        public int y { get; set; }
    }

    public class Ball
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}