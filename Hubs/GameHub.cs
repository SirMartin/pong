﻿using System.Collections.Generic;
using Pong.Models;
using System.Linq;
using Microsoft.AspNet.SignalR;

namespace Pong.Hubs
{
    public class GameHub : Hub
    {
        List<GameInfo> games;

        public GameHub()
        {
            games = new List<GameInfo>();
        }

        public void StartGame(string name)
        {
            var theGame = games.FirstOrDefault(g => g.player1 == null || g.player2 == null);
            if (theGame == null)
            {
                // New game.
                theGame = new GameInfo();
                theGame.player1 = new Player(name, Context.ConnectionId);
                games.Add(theGame);

                Clients.Caller.getGameInfo(theGame);
            }
            else
            {
                // Only 1 empty.
                if (theGame.player1 == null)
                    theGame.player1 = new Player(name, Context.ConnectionId);
                else
                    theGame.player2 = new Player(name, Context.ConnectionId);

                theGame.IsStarted = true;

                var myUsers = new List<string> { theGame.player1.id, theGame.player2.id };
                Clients.Users(myUsers).getGameInfo(theGame);
            }            
        }

        public void SendInfo(GameInfo gameInfo)
        {
            var theGame = games.Single(g => g.Id == gameInfo.Id);
            theGame = gameInfo;
            var myUsers = new List<string> { gameInfo.player1.id, gameInfo.player2.id };
            Clients.Users(myUsers).getGameInfo(gameInfo);
        }
    }
}