﻿module Global {
    export class GameInfo {

        id: string;
        player1: Player;
        player2: Player;
        ball: Ball;
        isStarted: boolean;

        constructor() {
            this.player1 = null;
            this.player2 = null;
            this.ball = new Ball();
            this.isStarted = false;
        }
    }
}