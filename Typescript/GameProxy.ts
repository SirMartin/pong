﻿interface SignalR {
    gameHub: any;
}

interface GameProxy {
    client: GameClient;
    server: GameServer;
}

interface GameClient {
    getGameInfo: (g: Global.GameInfo) => void;
    get: (g: string) => void;
}

interface GameServer {
    startGame: (name: string) => void;
    sendInfo(g: Global.GameInfo):void;
}