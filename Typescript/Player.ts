﻿module Global {
    export class Player {

        id: string;
        name: string;
        score: KnockoutObservable<number>;
        y: KnockoutObservable<number>;

        constructor(id: string, name: string) {
            this.id = id;
            this.name = name;
            this.score = ko.observable(0);
            this.y = ko.observable(0);
        }
    }
}