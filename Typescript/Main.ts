﻿module Global {
    export class Main {

        // Get hub.
        gameHub: GameProxy = $.connection.gameHub;

        playerNumber: KnockoutComputed<number>;
        playerName: KnockoutComputed<string>;

        gameInfo: KnockoutObservable<GameInfo>;

        constructor() {
            this.gameInfo = ko.observable(null);

            // Wait for 2 players.
            var name = prompt("Enter your name");

            this.playerNumber = ko.computed(() => {
                if (this.gameInfo() != null && (this.gameInfo().player1 != null || this.gameInfo().player2 != null)) {
                    if (this.gameInfo().player1.id == $.connection.hub.id)
                        return 1;
                    else
                        return 2;
                }
                return 0;
            });

            this.playerName = ko.computed(() => {
                if (this.gameInfo() != null && (this.gameInfo().player1 != null || this.gameInfo().player2 != null)) {
                    if (this.gameInfo().player1.id == $.connection.hub.id)
                        return this.gameInfo().player1.name;
                    else
                        return this.gameInfo().player2.name;
                }
                return "";
            });

            $.connection.hub.start().done(() => {
                this.gameHub.server.startGame(name);                
            });
            
            this.gameHub.client.getGameInfo = (g: GameInfo) => {
                this.gameInfo(g);

                if (this.gameInfo().isStarted)
                    this.startGame();
            };
        }
        
        sendInfo() {
            this.gameHub.server.sendInfo(this.gameInfo());
        }

        startGame() {
            alert("STARTING AN AMAZING GAME NOW!");
        }
    }
}