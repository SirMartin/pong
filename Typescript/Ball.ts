﻿module Global {
    export class Ball {

        x: KnockoutObservable<number>;
        y: KnockoutObservable<number>;

        constructor() {
            this.x = ko.observable(0);
            this.y = ko.observable(0);
        }
    }
}