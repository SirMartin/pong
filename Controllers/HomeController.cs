﻿using System.Web.Mvc;

namespace Pong.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Server()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}